## REACT JS - IONIC - FIREBASE - CAPACITOR

## Available Scripts

In the project directory, you can run:

<!-- ### `npm i -g @ionic/react @ionic/react-router ionic react react-dom react-router react-router-dom`
First thing todo after clone repository. -->
### `npm install`
First thing todo after cloning project from gitlab.

### `npm run build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npx cap add ( android / ios )`

Builds the app for production to the 'android/ios' folder for open it with your mobile dev software.
ex : `npx cap add android`.

### `npx cap run (android / ios) `

Before you deploy it as Android Apps Installer (.Apk), open file capacitor.config.json
and then at the end of code change the path of your Android Studio.
* "LinuxAndroidStudioPath": YOUR_PATH_GOES_HERE
* "WindowsAndroidStudioPath": YOUR_PATH_GOES_HERE
After changing the path, type `npx cap run android` on your terminal