import React, { useEffect, useState } from 'react';
import { IonApp, IonTabs, IonRouterOutlet, IonTabBar, IonTabButton, IonLabel, IonIcon, IonBadge } from "@ionic/react";
import { IonReactRouter } from '@ionic/react-router';
import { home, cart, person } from 'ionicons/icons';
import { Route, Redirect } from 'react-router';

/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';

/* Theme variables */
import './theme/variables.css';

// User
import Home from "./pages/Home";
import Cart from "./pages/Cart";
import Account from "./pages/Account";
import Details from './pages/Details';
import Beli from './pages/process/Beli';
import Login from './pages/Login';
import Register from './pages/Register';
import { ProvideApp, useUtils } from './data/utils';
import Userdata from './pages/account/Userdata';
import PasswordPage from './pages/account/Password';
import Pending from './pages/account/Pending';
import History from './pages/account/History';

const App: React.FC = () => {
  return (
    <ProvideApp>
      <IonicUser />
    </ProvideApp>
  );
}

const IonicUser: React.FC = () => {
  const utils = useUtils();
  const [badge, setBadge] = useState<any[]>([]);

  useEffect(() => {
    async function showBadge() {
      setBadge(b => b.splice(0, 100))
      await utils.getCart(utils.user.state.uid);
      setTimeout(() => {
        setBadge(utils.cart);
      }, 2000);
    }

    if (!utils.user.initializing) {
      if (utils.user.state !== null) {
        showBadge();
      }
    }
  }, [utils]);

  return (
    <IonApp>
      <IonReactRouter>
        <IonTabs>
          <IonRouterOutlet>
            <Route path="/Home" component={Home} exact={true} />
            <Route path="/Cart" component={Cart} exact={true} />
            <Route path="/Details/:id" component={Details} />
            <Route path="/Details" render={() => <Redirect to="/Home" />} exact={true} />
            <Route path="/Beli" render={() => <Redirect to="/Home" />} exact={true} />
            <Route path="/Beli/:id" component={Beli} exact={true} />
            <Route path="/" render={() => <Redirect to="/Home" />} exact={true} />
            <Route path="/Account" component={Account} />
            <Route path="/Register" component={Register} exact={true} />
            <Route path="/Login" component={Login} exact={true} />
            <Route path="/Userdata" component={Userdata} exact={true} />
            <Route path="/Password" component={PasswordPage} exact={true} />
            <Route path="/Pending" component={Pending} exact={true} />
            <Route path="/History" component={History} exact={true} />
          </IonRouterOutlet>

          <IonTabBar slot="bottom">
            <IonTabButton tab="Home" href="/Home">
              <IonIcon icon={home} />
              <IonLabel>Beranda</IonLabel>
            </IonTabButton>
            <IonTabButton tab="Cart" href="/Cart">
              {!utils.user.initializing && utils.user.state !== null ? <IonBadge>{badge.length}</IonBadge> : ''}
              <IonIcon icon={cart} />
              <IonLabel>Keranjang</IonLabel>
            </IonTabButton>
            <IonTabButton tab="Account" href="/Account">
              <IonIcon icon={person} />
              <IonLabel>Akun</IonLabel>
            </IonTabButton>
          </IonTabBar>
        </IonTabs>
      </IonReactRouter>
    </IonApp >
  )
}

export default App;