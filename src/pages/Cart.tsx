import React, { useState, useEffect } from "react";
import { IonPage, IonHeader, IonContent, IonTitle, IonToolbar, IonCheckbox, IonList, IonItem, IonLabel, IonGrid, IonCol, IonRow, IonText, IonButton, IonIcon, IonButtons, IonBackButton, IonCard, IonFabButton, IonAlert, IonRefresher, IonRefresherContent, IonLoading, IonModal, IonCardContent, IonCardHeader, IonImg, IonCardSubtitle, IonCardTitle, IonInput, IonToast } from "@ionic/react";
import { useHistory } from "react-router";
import { basket, cash, returnLeft, remove, add, trash, close } from "ionicons/icons";
import './Home.css';
import { useUtils } from "../data/utils";
import { RefresherEventDetail } from "@ionic/core";


const Cart: React.FC = () => {
  const utils = useUtils();
  let history = useHistory();
  const [cart, setCart] = useState<any[]>([]);
  const [isi, setIsi] = useState<any[]>([]);
  const [penerima, setPenerima] = useState({
    nama: '',
    alamat: '',
    tlp: '',
  })

  const [loading, setLoading] = useState(false);
  const [check, setCheck] = useState(0);
  const [alert, setAlert] = useState(false);
  const [alert2, setAlert2] = useState(false);
  const [alert3, setAlert3] = useState(false);
  const [modal, setModal] = useState(false);
  const [state, setState] = useState(0);
  const [stateUser, setStateUser] = useState(0);
  const [total, setTotal] = useState(0);
  const [hide1, setHide1] = useState(false);
  const [hide2, setHide2] = useState(true);
  const [toast, setToast] = useState(false);

  useEffect(() => {
    setLoading(true);
    function show() {
      setCart(utils.cart);
      setLoading(false);
    }

    if (!utils.user.initializing) {
      if (utils.user.state !== null) {
        show();
      } else {
        setLoading(false);
        setAlert2(true);
      }
    }
  }, [state, utils]);

  useEffect(() => {
    async function userData() {
      try {
        await utils.getDataUser(utils.user.state.uid);
        setPenerima({
          nama: utils.dataUser[0].name,
          alamat: utils.dataUser[0].address,
          tlp: utils.dataUser[0].phone
        })
      } catch (err) {
        console.error(err)
      }
    }
    if (!utils.user.initializing && utils.user.state !== null) {
      userData();
    }
  }, [utils, stateUser])

  useEffect(() => {
    setLoading(true);
    async function getTotal() {
      let temp = 0
      let tempTtl = 0
      cart.map((x) => {
        if (x.check === true) {
          temp = x.harga * x.items;
          tempTtl = tempTtl + temp;
        }
        return tempTtl;
      });
      setTotal(tempTtl);


      setLoading(false);
    }
    getTotal();
  }, [cart]);

  const checkout = async () => {
    let arr: any[] = [];
    cart.map(x => {
      if (x.check) {
        arr.push({
          id: x.id,
          nama: x.nama,
          foto: x.foto,
          jumlah: x.items,
          harga: x.harga,
          hargaTotal: x.hargaTotal
        });
      }
      return arr;
    })
    setIsi(arr);
    setModal(true);
  }

  const bayar = async () => {
    try {
      setLoading(true);
      await utils.bayar(utils.user.state.uid, isi, penerima, total);
      try {
        isi.forEach(async x => {
          await utils.updateStock(x.id, x.jumlah);
          await utils.delCart(x.id);
        })
      } catch (err) {
        console.error(err);
      }
      setTimeout(() => {
        setLoading(false);
        setAlert3(true);
      }, 1000)
    } catch (err) {
      console.error(err)
    }
  }

  const checkbox = (chk: boolean, index: any, stok: number) => {
    if (!chk) {
      if (stok > 0) {
        setCart(cart.map(x => {
          if (x.id !== index) return x
          return { ...x, check: true }
        }))
        setCheck(check + 1);
      } else {
        setToast(true)
      }
    } else {
      setCart(cart.map(x => {
        if (x.id !== index) return x
        return { ...x, check: false }
      }))
      setCheck(check - 1);
    }
  }

  const jumlah = (tipe: any, index: any, nilai: any, batas: any) => {
    if (tipe === "increment") {
      if (nilai !== batas) {
        setCart(cart.map(x => {
          if (x.id !== index) return x
          return { ...x, items: x.items + 1, hargaTotal: x.harga * (x.items + 1) }
        }))
      }
    } else {
      if (nilai !== batas) {
        setCart(cart.map(x => {
          if (x.id !== index) return x
          return { ...x, items: x.items - 1, hargaTotal: x.harga * (x.items - 1) }
        }))
      }
    }
  }

  const doRefresh = (event: CustomEvent<RefresherEventDetail>) => {
    setTimeout(() => {
      setState(state + 1);
      event.detail.complete();
    }, 1000);
  }

  return (

    <IonPage>
      <IonModal isOpen={modal}>
        <IonHeader translucent>
          <IonToolbar>
            <IonTitle>Checkout</IonTitle>
            <IonButtons slot="end">
              <IonButton onClick={() => {
                setStateUser(stateUser + 1);
                setModal(false);
                setHide1(false);
                setHide2(true);
              }} >
                <IonLabel>Batal</IonLabel>
                <IonIcon icon={close} />
              </IonButton>
            </IonButtons>
          </IonToolbar>
        </IonHeader>

        <IonContent>
          <IonCard>
            <IonCardHeader>
              <IonCardTitle>Alamat</IonCardTitle>
            </IonCardHeader>

            <IonCardContent hidden={hide1}>
              <form onSubmit={(e) => {
                e.preventDefault();
                setHide1(true);
                setHide2(false);
              }}>
                <IonItem>
                  <IonLabel position="floating" >Nama Penerima</IonLabel>
                  <IonInput
                    name="nama"
                    type="text"
                    value={penerima.nama}
                    required
                    onIonChange={e =>
                      setPenerima({ ...penerima, nama: e.detail.value! })}
                  />
                </IonItem>

                <IonItem>
                  <IonLabel position="floating" >No Telepon</IonLabel>
                  <IonInput
                    name="tlp"
                    type="text"
                    value={penerima.tlp}
                    required
                    onIonChange={e =>
                      setPenerima({ ...penerima, tlp: e.detail.value! })}
                  />
                </IonItem>

                <IonItem>
                  <IonLabel position="floating" >Alamat</IonLabel>
                  <IonInput
                    name="alamat"
                    type="text"
                    value={penerima.alamat}
                    required
                    onIonChange={e =>
                      setPenerima({ ...penerima, alamat: e.detail.value! })}
                  />
                </IonItem>

                <IonGrid>
                  <IonRow>
                    <IonCol></IonCol>
                    <IonCol size={"6"}>
                      <IonButton
                        color="danger"
                        fill="outline"
                        type="submit"
                      >
                        <IonLabel>Selanjutnya</IonLabel>
                      </IonButton>
                    </IonCol>
                  </IonRow>
                </IonGrid>
              </form>
            </IonCardContent>
          </IonCard>

          <IonCard>
            <IonCardHeader>
              <IonCardTitle>Barang</IonCardTitle>
            </IonCardHeader>
            <IonCardContent hidden={hide2}>
              {isi.map(val => {
                return (
                  <IonItem key={val.id}>
                    <IonGrid>
                      <IonRow>
                        <IonCol size={"3"}>
                          <IonImg alt="" src={val.foto} />
                        </IonCol>
                        <IonCol>
                          <IonLabel>{val.nama}</IonLabel>
                          <IonCardSubtitle>x{val.jumlah}</IonCardSubtitle>
                          <IonLabel color="danger"> Rp. {val.hargaTotal.toLocaleString()}</IonLabel>
                        </IonCol>
                      </IonRow>
                    </IonGrid>
                  </IonItem>
                )
              })}

              <IonGrid>
                <IonRow>
                  <IonCol></IonCol>
                  <IonCol size={"7"}>
                    Total Harga : <IonLabel color="danger"> Rp. {total.toLocaleString()}</IonLabel>
                  </IonCol>
                </IonRow>
                <IonRow>
                  <IonCol size={"6"}>
                    <IonButton
                      expand="block"
                      color="danger"
                      fill="outline"
                      onClick={() => {
                        setHide1(false);
                        setHide2(true);
                      }}
                    >
                      <IonLabel class="ion-text-wrap"> Kembali </IonLabel>
                    </IonButton>
                  </IonCol>
                  <IonCol size={"6"}>
                    <IonButton
                      expand="block"
                      color="danger"
                      fill="outline"
                      onClick={() => {
                        setAlert(true)
                      }}
                    >
                      <IonLabel class="ion-text-wrap"> Beli </IonLabel>
                    </IonButton>
                  </IonCol>
                </IonRow>
              </IonGrid>
            </IonCardContent>
          </IonCard>
        </IonContent>
      </IonModal>

      <IonHeader translucent>
        <IonToolbar>
          <IonButtons>
            <IonBackButton icon={returnLeft} color="danger" defaultHref={"/Home"} />
            <IonTitle>Keranjang</IonTitle>
          </IonButtons>
        </IonToolbar>
      </IonHeader>

      <IonContent>
        <IonLoading
          isOpen={loading}
          message={"Loading..."}
        />

        <IonRefresher slot="fixed" onIonRefresh={doRefresh}>
          <IonRefresherContent></IonRefresherContent>
        </IonRefresher>

        <IonToast
          isOpen={toast}
          position="middle"
          onDidDismiss={() => setToast(false)}
          message="Stok Habis"
          duration={1000}
          buttons={[
            {
              side: 'end',
              text: 'tutup',
              handler: () => {
                setToast(false)
              }
            }
          ]}
        />

        <IonAlert
          isOpen={alert}
          header={"Apakah anda yakin?"}
          buttons={[
            {
              text: "Batal",
              handler: () => setAlert(false)
            },
            {
              text: "Ya",
              handler: () => {
                bayar();
                setAlert(false);
                setModal(false);
                setHide1(false);
                setHide2(true);
              }
            },
          ]}
        />

        <IonAlert
          isOpen={alert2}
          header={'Silahkan Login Terlebih Dahulu'}
          buttons={[
            {
              text: 'Lain Kali',
              role: "cancel",
              handler: () => {
                history.replace("/Home");
              }
            },
            {
              text: 'Login',
              handler: () => {
                history.replace("/Login");
              }
            }
          ]}
        />

        <IonAlert
          isOpen={alert3}
          header={'Transaksi berhasil'}
          buttons={[
            {
              text: 'OK',
              role: "cancel",
              handler: () => {
                history.replace("/Pending")
              }
            }
          ]}
        />



        {cart.map((item: any) => {
          return (
            <IonItem key={item.id}>
              <IonCard>
                <IonRow>
                  <IonCol>
                    <IonItem>
                      <IonCheckbox
                        checked={item.check}
                        color="danger"
                        slot="start"
                        onClick={() => checkbox(item.check, item.id, item.stok)}
                      />
                      <img src={item.foto} alt="" />
                    </IonItem>
                    <IonItem>
                      <IonCol>
                        <IonFabButton disabled={false} size={"small"} onClick={() => {
                          jumlah("decrement", item.id, item.items, 1);
                        }
                        }
                          color="danger">
                          <IonIcon icon={remove} />
                        </IonFabButton>
                      </IonCol>
                      <u>{item.items}</u>
                      <IonCol>
                        <IonFabButton size={"small"} onClick={() => {
                          jumlah("increment", item.id, item.items, item.stok);
                        }} color="danger">
                          <IonIcon icon={add} />
                        </IonFabButton>
                      </IonCol>
                    </IonItem>
                  </IonCol>
                  <IonCol>
                    <IonText>{item.nama}</IonText>
                    <br />
                    <br /><IonText color="danger"><IonIcon color="success" icon={cash}></IonIcon>
                      Rp. {item.hargaTotal.toLocaleString()}</IonText>
                    <br />
                    <IonList>
                      <IonButton
                        color="danger"
                        onClick={async () => {
                          await utils.delCart(item.id);
                          history.go(0);
                        }}
                      >
                        <IonIcon icon={trash}></IonIcon>
                      </IonButton>
                      <br />
                    </IonList>

                    {item.stok < 1 ? <IonLabel color="danger">*Stok Habis</IonLabel> : ''}
                  </IonCol>
                </IonRow>
              </IonCard>
            </IonItem>
          );
        }
        )}

        <IonGrid>
          <IonRow>
            <IonCol col-16>
              <IonLabel>
                <IonText><h2>Total Harga</h2></IonText>
                <IonText color="danger">
                  <IonIcon color="success" icon={cash}></IonIcon>
                  {

                  }
                  Rp. {total.toLocaleString()}
                </IonText>
              </IonLabel>
            </IonCol>
            <IonCol col-16 >
              <IonLabel>
                <IonButton expand="full" color="danger" onClick={() => checkout()} disabled={check === 0 ? true : false}>
                  <IonIcon icon={basket}> </IonIcon>
                  <IonCol>Beli</IonCol>
                </IonButton>
              </IonLabel>
            </IonCol>
          </IonRow>
        </IonGrid>
      </IonContent>

    </IonPage>
  );
}

export default Cart;