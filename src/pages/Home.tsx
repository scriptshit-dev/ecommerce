import React, { useEffect, useState } from "react";
import { IonPage, IonContent, IonCard, IonCardContent, IonSlides, IonSlide, IonCardHeader, IonCardSubtitle, IonRow, IonCol, IonIcon, IonSpinner, IonGrid, IonRefresher, IonRefresherContent, IonSegment, IonSegmentButton, IonLabel, IonText } from "@ionic/react";
import Char1 from './../assets/images/char1.png';
import Char2 from './../assets/images/char2.png';
import Char3 from './../assets/images/char3.jpeg';
import slideOpts from "../assets/function/Carousel.js";
import './Home.css';
import { cash } from "ionicons/icons";
import { useUtils } from "../data/utils";
import { RefresherEventDetail } from "@ionic/core";

const Home: React.FC = () => {
  const utils = useUtils();
  const [segment, setSegment] = useState('buah');
  const [buah, setBuah] = useState<any[]>([]);
  const [sayur, setSayur] = useState<any[]>([]);
  const [state, setState] = useState(0);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    setLoading(true);
    async function show() {
      
      setBuah(b => b.splice(0, 100));
      await utils.getBuah();
      setBuah(utils.buah);

      setSayur(s => s.splice(0, 100));
      await utils.getSayur();
      setSayur(utils.sayur);
      
      setLoading(false);
    }
    if(utils.user.initializing === false) {
      show();
    }
  }, [state, utils]);

  function waitContent() {
    let dataBarang: any = [];
    for (let i = 0; i < buah.length; i++) {
      dataBarang.push(
        <IonCard className="ListHeader" key={i}>
          <IonCardContent className="loadContent">
            {loading ? <IonSpinner className="loading" name="circles" /> : ''}
          </IonCardContent>
        </IonCard>);
    }
    return dataBarang;
  }

  function doRefresh(event: CustomEvent<RefresherEventDetail>) {
    setState(state + 1);
    setTimeout(() => {
      event.detail.complete();
    }, 1000);
  }

  return (
    <IonPage>
      <IonContent fullscreen>
        <IonRefresher slot="fixed" onIonRefresh={doRefresh}>
          <IonRefresherContent></IonRefresherContent>
        </IonRefresher>
        <IonCard>
            <IonSlides pager={true} options={slideOpts}>
              <IonSlide>
                <img src={Char1} alt="" />
              </IonSlide>
              <IonSlide>
                <img src={Char2} alt="" />
              </IonSlide>
              <IonSlide>
                <img src={Char3} alt="" />
              </IonSlide>
            </IonSlides>
        </IonCard>

        <IonSegment value={segment} onIonChange={e => setSegment(e.detail.value!)}>
          <IonSegmentButton value="buah" >
            <IonLabel>Buah</IonLabel>
          </IonSegmentButton>
          <IonSegmentButton value="sayur">
            <IonLabel>Sayur</IonLabel>
          </IonSegmentButton>
        </IonSegment>

        <IonGrid>
          <IonRow>
            {
              loading
                ? waitContent()
                : segment === 'buah'
                  ? buah.map((item: any) => {
                    return (
                      <IonCol size="6" key={item.id}>
                        <IonCard type="button" button={true} href={"/Details/" + item.id} >
                          <IonCardHeader className="cardheader" >
                            <img alt="" src={item.foto} />
                          </IonCardHeader>
                          <IonCardContent className="CardContent">
                            <IonCardSubtitle>{item.nama}</IonCardSubtitle>
                            <IonText>Stok : {item.stok}</IonText>
                          </IonCardContent>
                          <IonRow className="cardfooter">
                            <IonCol>
                              <IonCardSubtitle color="danger">
                                <IonIcon color="success" icon={cash} className="cash-icon" />
                                Rp. {item.harga.toLocaleString()}
                              </IonCardSubtitle>
                            </IonCol>
                          </IonRow>
                        </IonCard>
                      </IonCol>
                    )
                  })
                  : sayur.map((item: any) => {
                    return (
                      <IonCol size="6" key={item.id}>
                        <IonCard type="button" button={true} href={"/Details/" + item.id} >
                          <IonCardHeader className="cardheader" >
                            <img alt="" src={item.foto} />
                          </IonCardHeader>
                          <IonCardContent className="CardContent">
                            <IonCardSubtitle>{item.nama}</IonCardSubtitle>
                            <IonText>Stok : {item.stok}</IonText>
                          </IonCardContent>
                          <IonRow className="cardfooter">
                            <IonCol>
                              <IonCardSubtitle color="danger">
                                <IonIcon color="success" icon={cash} className="cash-icon" />
                                Rp. {item.harga.toLocaleString()}
                              </IonCardSubtitle>
                            </IonCol>
                          </IonRow>
                        </IonCard>
                      </IonCol>
                    )
                  })
            }
          </IonRow>
        </IonGrid>
      </IonContent>
    </IonPage>
  )
}

export default Home;