import React, { useState, useEffect } from "react";
import { IonPage, IonLabel, IonCardContent, IonCardHeader, IonCardTitle, IonContent, IonGrid, IonCol, IonRow, IonCard, IonBackButton, IonButtons, IonToolbar, IonText, IonButton, IonIcon, IonSpinner, IonAlert, IonRefresher, IonRefresherContent, IonLoading, } from "@ionic/react";
import { useParams, useHistory } from "react-router";
import { returnLeft, basket, heart, cash, people } from "ionicons/icons";
import './Details.css';
import { useUtils } from "../data/utils";
import { RefresherEventDetail } from "@ionic/core";


const Details: React.FC = () => {
  let { id } = useParams();
  let history = useHistory();
  const utils = useUtils();
  const [detail, setDetail] = useState<any[]>([]);
  const [state, setState] = useState(0);
  const [loading, setLoading] = useState(true);
  const [showAlert, setShowAlert] = useState(false);
  const [dis, setDis] = useState(false);

  useEffect(() => {
    setLoading(true);
    async function show() {
      setDetail(d => d.splice(0, 100))
      await utils.getDetail(id);
      setDetail(utils.detail);
      if (utils.detail[0].stok < 1) {
        setDis(true)
      }
      setLoading(false);
    }
    if (!utils.user.initializing) {
      show();
    }
  }, [state, id, utils]);

  function waitContent() {
    return(
      <IonCard>
        <IonCardHeader className="ListHeaderDetail">
          <IonCardTitle>
            
          </IonCardTitle>
        </IonCardHeader>
          <IonCardContent>
          {loading ? <IonSpinner name="circles" /> : ''}  
          </IonCardContent>
      </IonCard>
    )
  }

  function doRefresh(event: CustomEvent<RefresherEventDetail>) {
    setState(state + 1);
    setTimeout(() => {
      event.detail.complete();
    }, 1000);
  }

  return (
    loading ? <IonLoading
      isOpen={loading}
      onDidDismiss={() => setLoading(false)}
      message={'Loading...'}
    /> :
      <IonPage>

        <IonToolbar>
          <IonRow>
            <IonButtons>
              <IonBackButton icon={returnLeft} color="danger" defaultHref="/" />
              <IonLabel>Kembali</IonLabel>
            </IonButtons>
          </IonRow>
        </IonToolbar>

        <IonContent>
          <IonRefresher slot="fixed" onIonRefresh={doRefresh}>
            <IonRefresherContent></IonRefresherContent>
          </IonRefresher>

          <IonAlert
            isOpen={showAlert}
            header={'Silahkan Login Terlebih Dahulu'}
            buttons={[
              {
                text: 'Lain Kali',
                handler: () => {
                  setShowAlert(false)
                }
              },
              {
                text: 'Login',
                handler: () => {
                  history.replace("/Login");
                }
              }
            ]}
          />

          {loading ? waitContent() :
            detail.map((item: any) => (
              <IonGrid key={item.id}>
                <IonCard>
                  <IonCardContent>
                    <img src={item.foto} alt="" />
                  </IonCardContent>
                </IonCard>
                <IonCard>
                  <IonCardHeader className="ListHeaderDetail">
                    <IonCardTitle>
                      {item.nama}
                    </IonCardTitle><br />
                    <IonGrid>
                      <IonRow>
                        <IonCol>
                          <IonLabel>
                            <IonText color="danger"><h2>
                              <IonIcon color="success" icon={cash}></IonIcon>
                              Rp.{item.harga.toLocaleString()}</h2></IonText>
                            <IonText><h3> Kota {item.penjual}</h3></IonText>
                          </IonLabel>
                        </IonCol>
                        <IonCol>
                          <IonLabel>
                            <IonText><h2>
                              <IonIcon icon={people}></IonIcon>  {item.kategori}</h2></IonText>
                          </IonLabel>
                        </IonCol>
                      </IonRow>
                      <IonRow>
                        <IonCol>
                          <IonText>No. Telpon {item.noHp}</IonText>
                        </IonCol>
                      </IonRow>
                    </IonGrid>
                  </IonCardHeader>
                </IonCard>
                <IonCard>
                  <IonCardContent>
                    <IonLabel>
                      <u><h1>Informasi Produk</h1></u>
                    </IonLabel>
                    {item.desc}
                  </IonCardContent>
                </IonCard>
                <IonGrid>
                  {dis ? <IonLabel color="danger">* Stok Habis</IonLabel> : '' }
                  <IonRow>
                    <IonCol col-16>
                      <IonButton
                        expand="full"
                        disabled={dis}
                        onClick={async () => {
                          if (utils.user.state !== null) {
                            await utils.toCart(utils.user.state.uid, item.id);
                            history.replace("/Cart");
                            history.go(0);
                          } else {
                            setShowAlert(true)
                          }
                        }}
                      >
                        <IonIcon icon={heart}> </IonIcon>
                        <IonLabel>Keranjang</IonLabel>
                      </IonButton>
                    </IonCol>

                    <IonCol col-16>
                      <IonButton
                        expand="full"
                        disabled={dis}
                        color="danger"
                        href={"/Beli/" + id}
                      >
                        <IonIcon icon={basket}> </IonIcon>
                        <IonLabel>Beli</IonLabel>
                      </IonButton>
                    </IonCol>
                  </IonRow>
                </IonGrid>
              </IonGrid>)
            )}
        </IonContent>
      </IonPage>


  );
}

export default Details;