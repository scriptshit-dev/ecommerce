import React, { useState, FormEvent } from 'react';
import { IonPage, IonContent, IonCard, IonCardHeader, IonCardTitle, IonCardContent, IonItem, IonLabel, IonInput, IonButton, IonRow, IonCol, IonAlert, IonText, AlertButton, IonLoading } from '@ionic/react';
import './Login.css';
import { useHistory } from 'react-router';
import { useUtils } from '../data/utils';

interface IAlert {
  open: boolean,
  header: string,
  message: string,
  buttons: (AlertButton | string)[]
}

const initialAlert = {
  open: false,
  header: '',
  message: '',
  buttons: [],
}

const Login: React.FC = () => {
  const utils = useUtils();
  let history = useHistory();
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [alert, setAlert] = useState<IAlert>(initialAlert);
  const [loading, setLoading] = useState(false);

  const signin = async (e: FormEvent) => {
    e.preventDefault();
    setLoading(true);

    try {
      await utils.signin(email, password);
      setLoading(false);
      setAlert({
        open: true,
        header: "Berhasil Login",
        message: '',
        buttons: [{
          text: "OK",
          handler: () => {
            history.replace("/Home")
            history.go(0)
          }
        }]
      })
    } catch (err) {
      console.error(err);
      setLoading(false);
      switch (err.code) {
        case "auth/wrong-password":
          setAlert({
            open: true,
            header: "Gagal Login",
            message: "Login Gagal",
            buttons: ["OK"]
          })
          break;
        case "auth/user-not-found":
          setAlert({
            open: true,
            header: "Gagal Login",
            message: "Login Gagal",
            buttons: ["OK"]
          })
          break;
        default:
          break;
      }
    }
  }

  return (
    <IonPage>
      <IonContent>
        <IonLoading 
          isOpen={loading}
          message={"Loading..."}
        />

        <IonAlert
          isOpen={alert.open}
          header={alert.header}
          message={alert.message}
          onDidDismiss={() => setAlert({ ...alert, open: false })}
          buttons={alert.buttons}
        />

        <IonCard className="logCard">
          <IonCardHeader>
            <IonCardTitle>Silahkan Login</IonCardTitle>
          </IonCardHeader>

          <IonCardContent>
            <form onSubmit={signin}>
              <IonItem>
                <IonLabel position="floating" >Email</IonLabel>
                <IonInput
                  name="email"
                  type="email"
                  value={email}
                  onIonChange={e =>
                    setEmail(e.detail.value!)}
                />
              </IonItem>

              <IonItem>
                <IonLabel position="floating" >Password</IonLabel>
                <IonInput
                  name="password"
                  type="password"
                  value={password}
                  onIonChange={e =>
                    setPassword(e.detail.value!)}
                />
              </IonItem>

              <IonButton expand="block" shape="round" className="btn" type="submit"> Login </IonButton>
            </form>

            <IonRow class="cardfooter">
              <IonCol>
                <IonText>
                  <p>Belum Punya Akun? <a href="/Register">Buat Disini!</a></p>
                </IonText>
              </IonCol>
            </IonRow>
          </IonCardContent>
        </IonCard>
      </IonContent>
    </IonPage>
  );
}

export default Login;