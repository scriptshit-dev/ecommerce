import React, { useState, FormEvent } from 'react';
import { IonPage, IonContent, IonCard, IonCardHeader, IonCardTitle, IonCardContent, IonItem, IonLabel, IonInput, IonButton, IonAlert, IonRow, IonCol, IonText, AlertButton, IonLoading } from '@ionic/react';
import './Login.css';
import { useHistory } from 'react-router';
import { useUtils } from '../data/utils';

interface IAlert {
  open: boolean,
  header: string,
  message: string,
  buttons: (AlertButton | string)[]
}

const initialAlert = {
  open: false,
  header: '',
  message: '',
  buttons: [],
}

const Register: React.FC = () => {
  const utils = useUtils();
  let history = useHistory();
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [name, setName] = useState('');
  const [phone, setPhone] = useState('');
  const [address, setAddress] = useState('');
  const [alert, setAlert] = useState<IAlert>(initialAlert);
  const [loading, setLoading] = useState(false);

  const signup = async (e: FormEvent) => {
    e.preventDefault();
    setLoading(true);

    try {
      await utils.signup(email, password, name, address, phone);
      setLoading(false);
      setAlert({
        open: true,
        header: "Berhasil Membuat Akun",
        message: '',
        buttons: [{
          text: "OK",
          handler: () => {
            history.replace("/Home")
            history.go(0)
          }
        }]
      })
    } catch (err) {
      console.error(err);
      setLoading(false);
      switch (err.code) {
        case "auth/email-already-in-use":
          setAlert({
            open: true,
            header: "Gagal Membuat Akun",
            message: "Email Sudah Dipakai",
            buttons: ["OK"]
          })
          break;
        case "auth/invalid-email":
          setAlert({
            open: true,
            header: "Gagal Membuat Akun",
            message: "Silahkan masukan format email dengan benar",
            buttons: ["OK"]
          })
          break;
        case "auth/weak-password":
          setAlert({
            open: true,
            header: "Gagal Membuat Akun",
            message: "Password harus lebih dari 6 karakter",
            buttons: ["OK"]
          })
          break;
        default:
          break;
      }
    }
  }

  return (
    <IonPage>
      <IonContent>
        <IonLoading 
          isOpen={loading}
          message={"Loading..."}
        />
        
        <IonAlert
          isOpen={alert.open}
          header={alert.header}
          message={alert.message}
          onDidDismiss={() => setAlert({ ...alert, open: false })}
          buttons={alert.buttons}
        />

        <IonCard className="logCard">
          <IonCardHeader>
            <IonCardTitle>Pendaftaran</IonCardTitle>
          </IonCardHeader>

          <IonCardContent>
            <form onSubmit={signup}>
              <IonItem>
                <IonLabel position="floating" >Nama</IonLabel>
                <IonInput
                  name="name"
                  type="text"
                  value={name}
                  onIonChange={e =>
                    setName(e.detail.value!)}
                />
              </IonItem>

              <IonItem>
                <IonLabel position="floating" >No Telepon</IonLabel>
                <IonInput
                  name="phone"
                  type="text"
                  value={phone}
                  onIonChange={e =>
                    setPhone(e.detail.value!)}
                />
              </IonItem>

              <IonItem>
                <IonLabel position="floating" >Alamat</IonLabel>
                <IonInput
                  name="address"
                  type="text"
                  value={address}
                  onIonChange={e =>
                    setAddress(e.detail.value!)}
                />
              </IonItem>

              <IonItem>
                <IonLabel position="floating" >Email</IonLabel>
                <IonInput
                  name="email"
                  type="email"
                  value={email}
                  onIonChange={e =>
                    setEmail(e.detail.value!)}
                />
              </IonItem>

              <IonItem>
                <IonLabel position="floating" >Password</IonLabel>
                <IonInput
                  name="password"
                  type="password"
                  value={password}
                  onIonChange={e =>
                    setPassword(e.detail.value!)}
                />
              </IonItem>

              <IonButton expand="block" shape="round" className="btn" type="submit"> Daftar </IonButton>
            </form>

            <IonRow class="cardfooter">
              <IonCol>
                <IonText>
                  <p>Sudah Punya Akun? <a href="/Login">Klik Disini!</a></p>
                </IonText>
                <IonLabel>
                </IonLabel>
              </IonCol>
            </IonRow>

          </IonCardContent>
        </IonCard>
      </IonContent>
    </IonPage>
  );
}

export default Register;