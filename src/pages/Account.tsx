import React, { useState, useEffect } from "react";
import { IonPage, IonHeader, IonContent, IonTitle, IonToolbar, IonButton, IonAlert, IonLoading, IonItem, IonLabel, IonIcon, IonList } from "@ionic/react";
import { useHistory } from "react-router";
import { useUtils } from "../data/utils";
import { arrowDropright, arrowDropdown } from "ionicons/icons";

const accPage: any[] = [
  {
    title: "Akun",
    url: "",
    jenis: "Parent"
  },
  {
    title: "Data Diri",
    url: "/Userdata",
    jenis: "Child"
  },
  {
    title: "Ganti Password",
    url: "/Password",
    jenis: "Child"
  },
  {
    title: "Transaksi",
    url: "",
    jenis: "Parent"
  },
  {
    title: "Pembayaran Tertunda",
    url: "/Pending",
    jenis: "Child"
  },
  {
    title: "Catatan Pembelian",
    url: "/History",
    jenis: "Child"
  }
]

const Account: React.FC = () => {
  let history = useHistory();
  const utils = useUtils();
  const [loading, setLoading] = useState(true);
  const [alert, setAlert] = useState(false);
  const [hide1, setHide1] = useState(true);
  const [hide2, setHide2] = useState(true);

  useEffect(() => {
    if (!utils.user.initializing) {
      if (utils.user.state === null) {
        setLoading(false);
        setAlert(true);
      } else {
        setLoading(false);
      }
    }
  }, [utils]);

  const dropDown = (index: number) => {
    if (index === 0) {
      if (hide1 === true) {
        return <IonItem key={index} onClick={() => setHide1(false)}>
          Akun
        <IonIcon icon={arrowDropright} />
        </IonItem>;
      } else {
        return <IonItem key={index} onClick={() => setHide1(true)}>
          Akun
        <IonIcon icon={arrowDropdown}></IonIcon>
        </IonItem>;
      }
    } else {
      if (hide2 === true) {
        return <IonItem key={index} onClick={() => setHide2(false)}>
          Transaksi
        <IonIcon icon={arrowDropright} />
        </IonItem>;
      } else {
        return <IonItem key={index} onClick={() => setHide2(true)}>
          Transaksi
        <IonIcon icon={arrowDropdown}></IonIcon>
        </IonItem>;
      }
    }
  }

  return (
    loading ?
      <IonLoading
        isOpen={loading}
        message={'Loading...'}
      /> :
      <IonPage>
        <IonHeader>
          <IonToolbar>
            <IonTitle>
              Pengaturan Akun
          </IonTitle>
          </IonToolbar>
        </IonHeader>

        <IonContent>
          <IonAlert
            isOpen={alert}
            header={'Silahkan Login Terlebih Dahulu'}
            buttons={[
              {
                text: 'Lain Kali',
                role: "cancel",
                handler: () => {
                  history.replace("/Home");
                }
              },
              {
                text: 'Login',
                handler: () => {
                  history.replace("/Login");
                }
              }
            ]}
          />

          <IonList>

            {accPage.map((x, index) => {
              return (
                x.jenis === "Child" ?
                  <IonItem
                    hidden={index === 1 || index === 2 ? hide1 : hide2}
                    routerLink={x.url}
                    routerDirection="none"
                    key={x.title} >
                    <IonIcon />
                    <IonLabel>{x.title}</IonLabel>
                  </IonItem>
                  :
                  dropDown(index)
              );
            })}

          </IonList>
          <IonButton
            expand="block"
            color="danger"
            onClick={async () => {
              await utils.signout();
              history.replace("/home");
              history.go(0);
            }}>
            Logout
        </IonButton>
        </IonContent>
      </IonPage>
  );
}

export default Account;