import React, { useState, useEffect } from "react";
import { IonPage, IonLabel, IonContent, IonGrid, IonCol, IonRow, IonCard, IonBackButton, IonButtons, IonToolbar, IonText, IonList, IonItem, IonButton, IonIcon, IonTextarea, IonFabButton, IonAvatar, IonChip, IonHeader, IonRefresher, IonRefresherContent, IonAlert, IonLoading, IonModal, IonTitle, IonCardHeader, IonCardTitle, IonCardContent, IonInput, IonImg, IonCardSubtitle, } from "@ionic/react";
import { useParams, useHistory } from "react-router";
import { returnLeft, cash, add, remove, basket, close } from "ionicons/icons";
import { useUtils } from "../../data/utils";
import { RefresherEventDetail } from "@ionic/core";

const Beli: React.FC = () => {
  let { id } = useParams();
  let history = useHistory();
  const utils = useUtils();
  const [beli, setBeli] = useState<any[]>([]);
  const [isi, setIsi] = useState<any[]>([]);
  const [penerima, setPenerima] = useState({
    nama: '',
    alamat: '',
    tlp: '',
  })

  const [loading, setLoading] = useState(true);
  const [loading2, setLoading2] = useState(true);
  const [state, setState] = useState(0);
  const [stateUser, setStateUser] = useState(0);
  const [alert, setAlert] = useState(false);
  const [alert2, setAlert2] = useState(false);
  const [alert3, setAlert3] = useState(false);
  const [alert4, setAlert4] = useState(false);
  const [modal, setModal] = useState(false);
  const [hide1, setHide1] = useState(false);
  const [hide2, setHide2] = useState(true);
  const [total, setTotal] = useState(0);

  useEffect(() => {
    async function show() {
      setLoading(true);

      setBeli(b => b.splice(0, 100))
      await utils.getBeli(id);
      setBeli(utils.beli);

      setLoading(false);
    }
    if (!utils.user.initializing) {
      if (utils.user.state !== null) {
        show();
      } else {
        setLoading(false);
        setAlert3(true);
      }
    }
  }, [state, id, utils]);

  useEffect(() => {
    async function userData() {
      try {
        await utils.getDataUser(utils.user.state.uid);
        setPenerima({
          nama: utils.dataUser[0].name,
          alamat: utils.dataUser[0].address,
          tlp: utils.dataUser[0].phone
        })
      } catch (err) {
        console.error(err)
      }
    }
    if (!utils.user.initializing && utils.user.state !== null) {
      userData();
    }
  }, [utils, stateUser])

  const checkout = async () => {
    let arr2: any[] = [];
    beli.map(x => {
      setTotal(x.hargaTotal);
      arr2.push({
        id: x.id,
        nama: x.nama,
        foto: x.foto,
        jumlah: x.items,
        harga: x.harga,
        hargaTotal: x.hargaTotal,
        norek: x.norek
      });
      return {
        arr2
      }
    })
    setIsi(arr2);
    setModal(true);
  }

  const bayar = async () => {
    setLoading(true);
    try {
      await utils.bayar(utils.user.state.uid, isi, penerima, total);
      try {
        isi.forEach(async x => {
          await utils.updateStock(x.id, x.jumlah);
          await utils.delCart(x.id);
        })
      } catch (err) {
        console.error(err);
      }
      setTimeout(() => {
        setLoading(false);
        setAlert4(true);
      }, 1000)
    } catch (err) {
      console.error(err)
    }
  }

  const jumlah = (tipe: any, index: any, nilai: any, batas: any) => {
    if (tipe === "increment") {
      if (nilai !== batas) {
        setBeli(beli.map((x: any) => {
          if (x.id !== index) return x
          return { ...x, items: x.items + 1, hargaTotal: x.harga * (x.items + 1) }
        }))
      }
    } else {
      if (nilai !== batas) {
        setBeli(beli.map((x: any) => {
          if (x.id !== index) return x
          return { ...x, items: x.items - 1, hargaTotal: x.harga * (x.items - 1) }
        }))
      }
    }
  }

  function doRefresh(event: CustomEvent<RefresherEventDetail>) {
    setState(state + 1);
    setTimeout(() => {
      event.detail.complete();
    }, 1000);
  }

  return (
    loading ?
      <IonLoading
        isOpen={loading}
        message={'Loading...'}
      /> :
      <IonPage>
        <IonHeader>
          <IonToolbar>
            <IonButtons>
              <IonBackButton icon={returnLeft} color="danger" defaultHref={"/Details/" + id} />
              <IonLabel>Kembali</IonLabel>
            </IonButtons>
          </IonToolbar>
        </IonHeader>

        <IonContent>
          <IonRefresher slot="fixed" onIonRefresh={doRefresh}>
            <IonRefresherContent></IonRefresherContent>
          </IonRefresher>

          <IonAlert
            isOpen={alert2}
            header={"Apakah anda yakin?"}
            buttons={[
              {
                text: "Batal",
                handler: () => setAlert2(false)
              },
              {
                text: "Ya",
                handler: () => {
                  bayar();
                  setAlert2(false);
                  setModal(false);
                  setHide1(false);
                  setHide2(true);
                }
              },
            ]}
          />

          <IonAlert
            isOpen={alert3}
            header={'Silahkan Login Terlebih Dahulu'}
            buttons={[
              {
                text: 'Lain Kali',
                role: "cancel",
                handler: () => {
                  history.goBack();
                }
              },
              {
                text: 'Login',
                handler: () => {
                  history.replace("/Login");
                }
              }
            ]}
          />

          <IonAlert
            isOpen={alert4}
            header={'Transaksi berhasil'}
            buttons={[
              {
                text: 'OK',
                role: "cancel",
                handler: () => {
                  history.replace("/Pending")
                }
              }
            ]}
          />

          <IonModal isOpen={modal}>
            <IonHeader translucent>
              <IonToolbar>
                <IonTitle>Checkout</IonTitle>
                <IonButtons slot="end">
                  <IonButton onClick={() => {
                    setStateUser(stateUser + 1);
                    setModal(false);
                    setHide1(false);
                    setHide2(true);
                  }} >
                    <IonLabel>Batal</IonLabel>
                    <IonIcon icon={close} />
                  </IonButton>
                </IonButtons>
              </IonToolbar>
            </IonHeader>

            <IonContent>

              <IonCard>
                <IonCardHeader>
                  <IonCardTitle>Alamat</IonCardTitle>
                </IonCardHeader>

                <IonCardContent hidden={hide1}>
                  <form onSubmit={(e) => {
                    e.preventDefault();
                    setHide1(true);
                    setHide2(false);
                  }}>
                    <IonItem>
                      <IonLabel position="floating" >Nama Penerima</IonLabel>
                      <IonInput
                        name="nama"
                        type="text"
                        value={penerima.nama}
                        required
                        onIonChange={e =>
                          setPenerima({ ...penerima, nama: e.detail.value! })}
                      />
                    </IonItem>

                    <IonItem>
                      <IonLabel position="floating" >No Telepon</IonLabel>
                      <IonInput
                        name="tlp"
                        type="text"
                        value={penerima.tlp}
                        required
                        onIonChange={e =>
                          setPenerima({ ...penerima, tlp: e.detail.value! })}
                      />
                    </IonItem>

                    <IonItem>
                      <IonLabel position="floating" >Alamat</IonLabel>
                      <IonInput
                        name="alamat"
                        type="text"
                        value={penerima.alamat}
                        required
                        onIonChange={e =>
                          setPenerima({ ...penerima, alamat: e.detail.value! })}
                      />
                    </IonItem>

                    <IonGrid>
                      <IonRow>
                        <IonCol></IonCol>
                        <IonCol size={"6"}>
                          <IonButton
                            color="danger"
                            fill="outline"
                            type="submit"
                          >
                            <IonLabel>Selanjutnya</IonLabel>
                          </IonButton>
                        </IonCol>
                      </IonRow>
                    </IonGrid>
                  </form>

                </IonCardContent>
              </IonCard>

              <IonCard>
                <IonCardHeader>
                  <IonCardTitle>Barang</IonCardTitle>
                </IonCardHeader>
                <IonCardContent hidden={hide2}>
                  {isi.map(val => {
                    return (
                      <IonItem key={val.id}>
                        <IonGrid>
                          <IonRow>
                            <IonCol size={"3"}>
                              <IonImg alt="" src={val.foto} />
                            </IonCol>
                            <IonCol>
                              <IonLabel>{val.nama}</IonLabel>
                              <IonCardSubtitle>x{val.jumlah}</IonCardSubtitle>
                              <IonLabel color="danger"> Rp. {val.harga.toLocaleString()}</IonLabel>
                            </IonCol>
                          </IonRow>
                        </IonGrid>
                      </IonItem>
                    )
                  })}

                  <IonGrid>
                    <IonRow>
                      <IonCol></IonCol>
                      <IonCol size={"7"}>
                        Total Harga : <IonLabel color="danger"> Rp. {total.toLocaleString()}</IonLabel>
                      </IonCol>
                    </IonRow>
                    <IonRow>
                      <IonCol size={"6"}>
                        <IonButton
                          expand="block"
                          color="danger"
                          fill="outline"
                          onClick={() => {
                            setHide1(false);
                            setHide2(true);
                          }}
                        >
                          <IonLabel class="ion-text-wrap"> Kembali </IonLabel>
                        </IonButton>
                      </IonCol>
                      <IonCol size={"6"}>
                        <IonButton
                          expand="block"
                          color="danger"
                          fill="outline"
                          onClick={() => {
                            setAlert2(true)
                          }}
                        >
                          <IonLabel class="ion-text-wrap"> Beli </IonLabel>
                        </IonButton>
                      </IonCol>
                    </IonRow>
                  </IonGrid>

                </IonCardContent>
              </IonCard>
            </IonContent>
          </IonModal>


          {!utils.user.state ?
            <IonAlert
              isOpen={alert}
              onDidDismiss={() => setAlert(false)}
              header={"Silakan Login Terlebih Dahulu"}
              buttons={[
                {
                  text: "Batal",
                  handler: () => setAlert(false)
                },
                {
                  text: "Login",
                  handler: () => {
                    history.replace("/Login")
                  }
                }
              ]}
            /> :
            <IonAlert
              isOpen={alert}
              onDidDismiss={() => setAlert(false)}
              header={"Apakah anda yakin?"}
              buttons={[
                {
                  text: "Batal",
                  handler: () => setAlert(false)
                },
                {
                  text: "Ya",
                  handler: async () => {
                    await checkout();
                    history.replace("/Pending")
                  }
                }
              ]}
            />
          }

          {loading ? <IonLoading
            isOpen={loading2}
            onDidDismiss={() => setLoading2(false)}
            message={'Loading...'}
          /> :
            beli.map((item: any) => {
              return (
                item.id === id ?
                  <IonGrid key={item.id}>
                    <IonCard>
                      <IonRow>
                        <IonCol>
                          <img src={item.foto} alt=""></img>
                        </IonCol>

                        <IonCol>
                          <IonLabel>
                            <IonCol></IonCol>
                            <IonText><h2>{item.nama}</h2></IonText>
                            <IonCol></IonCol>
                            <IonText color="danger"><h2>
                              <IonIcon color="success" icon={cash}></IonIcon>
                              Rp. {item.harga.toLocaleString()}</h2></IonText>
                            <IonChip>
                              <IonAvatar color="red">
                                {/* <img src={item.avatar}></img> */}
                              </IonAvatar>
                              <IonLabel>{item.penjual}</IonLabel>
                            </IonChip>
                          </IonLabel>
                        </IonCol>
                      </IonRow>
                    </IonCard>

                    <IonCard>
                      <IonRow>
                        <IonCol>
                          <IonLabel>
                            <h2>Stok Terbatas! Tersisa {item.stok} </h2>
                          </IonLabel>
                        </IonCol>
                      </IonRow>
                      <IonList>
                        <IonItem>
                          <IonCol size={"6"}>
                            <IonLabel>Jumlah</IonLabel>
                          </IonCol>

                          <IonCol>
                            <IonFabButton disabled={false} size={"small"} onClick={() => {
                              jumlah("decrement", item.id, item.items, 1);
                            }
                            }
                              color="danger">
                              <IonIcon icon={remove} />
                            </IonFabButton>
                          </IonCol>
                          <u>{item.items}</u>
                          <IonCol>
                            <IonFabButton size={"small"} onClick={() => {
                              jumlah("increment", item.id, item.items, item.stok);
                            }} color="danger">
                              <IonIcon icon={add} />
                            </IonFabButton>
                          </IonCol>
                        </IonItem>
                      </IonList>

                      <IonItem>
                        <IonLabel position="stacked">Catatan Untuk Penjual</IonLabel>
                        <IonTextarea maxlength={99} placeholder="(Opsional)"></IonTextarea>
                      </IonItem>
                    </IonCard>

                    <IonGrid>
                      <IonRow>
                        <IonCol col-16>
                          <IonLabel>
                            <IonText><h2>Total Harga</h2></IonText>
                            <IonText color="danger">
                              <IonIcon color="success" icon={cash}></IonIcon>
                              {item.hargaTotal.toLocaleString()}
                            </IonText>
                          </IonLabel>
                        </IonCol>

                        <IonCol col-16 >
                          <IonLabel>
                            <IonButton expand="full" color="danger" onClick={() => checkout()}>
                              <IonIcon icon={basket}> </IonIcon>
                              <IonCol>Beli</IonCol>
                            </IonButton>
                          </IonLabel>
                        </IonCol>
                      </IonRow>
                    </IonGrid>
                  </IonGrid>
                  : ''
              );
            }
            )}
        </IonContent>
      </IonPage>
  );
}

export default Beli;