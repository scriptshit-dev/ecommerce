import React, { FormEvent, useState } from 'react';
import { useHistory } from 'react-router';
import { useUtils } from '../../data/utils';
import { IonItem, IonLabel, IonInput, IonButton, IonPage, IonContent, IonHeader, IonToolbar, IonTitle, IonIcon, IonRow, IonCard, IonCardContent } from '@ionic/react';
import { arrowBack } from 'ionicons/icons';

const PasswordPage: React.FC = () => {
  let history = useHistory();
  const utils = useUtils();
  const [pass, setPass] = useState('');

  const ubahPass = async (e: FormEvent) => {
    e.preventDefault();
    try {
      await utils.changePassword(pass);
      history.goBack();
    } catch (err) {
      console.error(err);
    }
  }

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonRow>
            <IonButton fill="clear" onClick={() => history.replace('/Account')}>
              <IonIcon icon={arrowBack} />
            </IonButton>
            <IonTitle> Ganti Password </IonTitle>
          </IonRow>
        </IonToolbar>
      </IonHeader>

      <IonContent>
        <IonCard>
          <IonCardContent>
            <form onSubmit={ubahPass}>
              <IonItem>
                <IonLabel position="stacked" >Password Baru</IonLabel>
                <IonInput
                  autofocus={true}
                  name="password"
                  type="password"
                  value={pass}
                  onIonChange={e =>
                    setPass(e.detail.value!)}
                />
              </IonItem>
              <IonButton expand="block" shape="round" className="btn" type="submit"> Ganti </IonButton>
            </form>
          </IonCardContent>
        </IonCard>
      </IonContent>
    </IonPage>
  )
}

export default PasswordPage;