import React, { useState, useEffect, useRef } from "react";
import { IonPage, IonHeader, IonContent, IonTitle, IonToolbar, IonButton, IonLoading, IonItem, IonLabel, IonInput, IonIcon, IonCol, IonGrid, IonRow, IonRefresher, IonRefresherContent } from "@ionic/react";
import { useHistory } from "react-router";
import { useUtils } from "../../data/utils";
import { create, checkmark, arrowBack } from "ionicons/icons";
import { RefresherEventDetail } from "@ionic/core";

const Userdata: React.FC = () => {
  let history = useHistory();
  const utils = useUtils();
  const [loading, setLoading] = useState(true);
  const [state, setState] = useState(0);
  const inputNama = useRef<HTMLIonInputElement>(null);
  const inputAlamat = useRef<HTMLIonInputElement>(null);
  const inputEmail = useRef<HTMLIonInputElement>(null);
  const inputTlp = useRef<HTMLIonInputElement>(null);

  const [dataUser, setDataUser] = useState({
    nama: '',
    alamat: '',
    email: '',
    tlp: ''
  })

  const [edit, setEdit] = useState({
    nama: true,
    alamat: true,
    email: true,
    tlp: true
  })

  const focusInput = (x: string) => {
    switch (x) {
      case "nama":
        if (inputNama.current) {
          inputNama.current.setFocus();
        }
        break;
      case "alamat":
        if (inputAlamat.current) {
          inputAlamat.current.setFocus();
        }
        break;
      case "tlp":
        if (inputTlp.current) {
          inputTlp.current.setFocus();
        }
        break;
      case "email":
        if (inputEmail.current) {
          inputEmail.current.setFocus();
        }
        break;
      default:
        break;
    }
  }

  useEffect(() => {
    setLoading(true);
    async function read() {
      if (utils.user.initializing === false) {
        await utils.getDataUser(utils.user.state.uid);
        setDataUser({
          nama: utils.dataUser[0].name,
          alamat: utils.dataUser[0].address,
          email: utils.dataUser[0].email,
          tlp: utils.dataUser[0].phone
        })
        setLoading(false);
      }
    }
    read();
  }, [state, utils])

  function doRefresh(event: CustomEvent<RefresherEventDetail>) {
    setState(state + 1);
    setTimeout(() => {
      event.detail.complete();
    }, 1000);
  }

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonRow>
            <IonButton fill="clear" onClick={() => history.replace('/Account')}>
              <IonIcon icon={arrowBack} />
            </IonButton>
            <IonTitle> Akun </IonTitle>
          </IonRow>
        </IonToolbar>
      </IonHeader>

      <IonContent>
        <IonRefresher slot="fixed" onIonRefresh={doRefresh}>
          <IonRefresherContent></IonRefresherContent>
        </IonRefresher>

        {loading ?
          <IonLoading
            isOpen={loading}
            message={'Loading...'}
            onDidDismiss={() => setLoading(false)}
          /> :
          <IonGrid>
            <IonItem>
              <IonRow>
                <IonCol>
                  <IonLabel position="floating">Nama</IonLabel>
                  <IonInput
                    type="text"
                    ref={inputNama}
                    value={dataUser.nama}
                    readonly={edit.nama}
                    onIonChange={e =>
                      setDataUser({ ...dataUser, nama: e.detail.value! })
                    }
                  />
                </IonCol>

                <IonCol size={"2"}>
                  {edit.nama ?
                    <IonButton
                      fill="clear"
                      onClick={() => {
                        setEdit({ ...edit, nama: false });
                        focusInput("nama");
                      }}>
                      <IonIcon icon={create} color="dark" />
                    </IonButton>
                    :
                    <IonButton
                      fill="clear"
                      onClick={() => {
                        utils.changeName(utils.user.state.uid, dataUser.nama);
                        setEdit({ ...edit, nama: true });
                      }}>
                      <IonIcon icon={checkmark} color="dark" />
                    </IonButton>
                  }
                </IonCol>
              </IonRow>
            </IonItem>

            <IonItem>
              <IonRow>
                <IonCol>
                  <IonLabel position="floating">No Telepon</IonLabel>
                  <IonInput
                    name="tlp"
                    ref={inputTlp}
                    value={dataUser.tlp}
                    readonly={edit.tlp}
                    onIonChange={e =>
                      setDataUser({ ...dataUser, tlp: e.detail.value! })
                    }
                  />
                </IonCol>

                <IonCol size={"2"}>
                  {edit.tlp ?
                    <IonButton
                      fill="clear"
                      onClick={() => {
                        setEdit({ ...edit, tlp: false });
                        focusInput("tlp");
                      }}>
                      <IonIcon icon={create} color="dark" />
                    </IonButton>
                    :
                    <IonButton
                      fill="clear"
                      onClick={() => {
                        utils.changePhone(utils.user.state.uid, dataUser.tlp);
                        setEdit({ ...edit, tlp: true });
                      }}>
                      <IonIcon icon={checkmark} color="dark" />
                    </IonButton>

                  }

                </IonCol>
              </IonRow>
            </IonItem>

            <IonItem>
              <IonRow>
                <IonCol>
                  <IonLabel position="floating">Alamat</IonLabel>
                  <IonInput
                    name="alamat"
                    ref={inputAlamat}
                    value={dataUser.alamat}
                    readonly={edit.alamat}
                    onIonChange={e =>
                      setDataUser({ ...dataUser, alamat: e.detail.value! })
                    }
                  />
                </IonCol>

                <IonCol size={"2"}>
                  {edit.alamat ?
                    <IonButton
                      fill="clear"
                      onClick={() => {
                        setEdit({ ...edit, alamat: false });
                        focusInput("alamat");
                      }}>
                      <IonIcon icon={create} color="dark" />
                    </IonButton>
                    :
                    <IonButton
                      fill="clear"
                      onClick={() => {
                        utils.changeAddress(utils.user.state.uid, dataUser.alamat);
                        setEdit({ ...edit, alamat: true });
                      }}>
                      <IonIcon icon={checkmark} color="dark" />
                    </IonButton>

                  }

                </IonCol>
              </IonRow>
            </IonItem>

            <IonItem>
              <IonRow>
                <IonCol>
                  <IonLabel position="floating">Email</IonLabel>
                  <IonInput
                    name="email"
                    type="email"
                    ref={inputEmail}
                    value={dataUser.email}
                    readonly={edit.email}
                    onIonChange={e =>
                      setDataUser({ ...dataUser, email: e.detail.value! })
                    }
                  />
                </IonCol>

                <IonCol size={"2"}>
                  {edit.email ?
                    <IonButton
                      fill="clear"
                      onClick={() => {
                        setEdit({ ...edit, email: false });
                        focusInput("email");
                      }}>
                      <IonIcon icon={create} color="dark" />
                    </IonButton>
                    :
                    <IonButton
                      fill="clear"
                      onClick={() => {
                        utils.changeEmail(dataUser.email);
                        setEdit({ ...edit, email: true });
                      }}>
                      <IonIcon icon={checkmark} color="dark" />
                    </IonButton>
                  }
                </IonCol>
              </IonRow>
            </IonItem>
          </IonGrid>
        }


      </IonContent>
    </IonPage>
  );
}

export default Userdata;