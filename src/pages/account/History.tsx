import React, { useState, useEffect } from 'react';
import { IonPage, IonHeader, IonToolbar, IonContent, IonSegment, IonSegmentButton, IonLabel, IonRow, IonButton, IonIcon, IonTitle, IonLoading, IonGrid, IonCol, IonImg, IonCardSubtitle, IonItemDivider, IonCard, IonCardHeader, IonCardContent, IonRefresher, IonRefresherContent, IonAlert } from '@ionic/react';
import { arrowBack } from 'ionicons/icons';
import { useHistory } from 'react-router';
import { useUtils } from '../../data/utils';
import { RefresherEventDetail } from '@ionic/core';

const History = () => {
  let utils = useUtils();
  let history = useHistory();
  const [segment, setSegment] = useState('shipping');
  const [loading, setLoading] = useState(true);
  const [state, setState] = useState(0);
  const [alert, setAlert] = useState(false);
  const [idTrans, setIDTrans] = useState('');
  const [shipping, setShipping] = useState<any[]>([]);
  const [complete, setComplete] = useState<any[]>([]);

  useEffect(() => {
    setLoading(true)
    async function show() {
      setShipping(s => s.splice(0, 100));
      await utils.getShipping();
      setShipping(utils.shipping);

      setComplete(c => c.splice(0, 100));
      await utils.getComplete();
      setComplete(utils.complete);

      setLoading(false);
    }
    if (utils.user.initializing === false) {
      show();
    }
  }, [state, utils]);

  function doRefresh(event: CustomEvent<RefresherEventDetail>) {
    setState(state + 1);
    setTimeout(() => {
      event.detail.complete();
    }, 1000);
  }

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonRow>
            <IonButton fill="clear" onClick={() => {
              history.replace('/Account');
              history.go(0);
            }}>
              <IonIcon icon={arrowBack} />
            </IonButton>
            <IonTitle> Catatan Pembelian </IonTitle>
          </IonRow>
        </IonToolbar>
      </IonHeader>

      {loading ?
        <IonLoading
          isOpen={loading}
          onDidDismiss={() => setLoading(false)}
          message={"Loading..."}
          duration={5000}
        /> :
        <IonContent>
          <IonRefresher slot="fixed" onIonRefresh={doRefresh}>
            <IonRefresherContent></IonRefresherContent>
          </IonRefresher>

          <IonAlert
            isOpen={alert}
            onDidDismiss={() => setAlert(false)}
            header={"Apakah barang sudah benar sampai tujuan?"}
            buttons={[
              {
                text: 'Tidak',
                handler: () => setAlert(false)
              },
              {
                text: 'Ya',
                handler: async () => {
                  await utils.shipToComp(utils.user.state.uid, idTrans);
                  setState(state + 1);
                }
              }
            ]}
          />

          <IonSegment value={segment} onIonChange={e => setSegment(e.detail.value!)}>
            <IonSegmentButton value="shipping">
              <IonLabel>Dalam Pengiriman</IonLabel>
            </IonSegmentButton>
            <IonSegmentButton value="complete">
              <IonLabel>Selesai</IonLabel>
            </IonSegmentButton>
          </IonSegment>

          {segment === 'shipping' ?
            shipping && shipping.length > 0 ?
              shipping.map((item: any) => {
                var isiShipping = item.isi.map((val: any) => {
                  return (
                    <IonGrid key={val.id}>
                      <IonRow>
                        <IonCol size={"3"}>
                          <IonImg alt="" src={val.foto} />
                        </IonCol>
                        <IonCol>
                          <IonLabel>{val.nama}</IonLabel>
                          <IonCardSubtitle>x{val.jumlah}</IonCardSubtitle>
                          <IonLabel color="danger"> Rp. {val.harga.toLocaleString()}</IonLabel>
                        </IonCol>
                      </IonRow>
                      <IonItemDivider />
                    </IonGrid>
                  )
                });
                return (
                  <IonCard key={item.id}>
                    <IonCardHeader>
                      <IonTitle> {new Date(item.date.toDate()).toLocaleString()} </IonTitle>
                    </IonCardHeader>
                    <IonCardContent>
                      {isiShipping}
                      <IonGrid>
                        <IonRow>
                          <IonCol size={"4"}>
                            Total Harga : <IonLabel color="danger">Rp. {item.total.toLocaleString()}</IonLabel>
                          </IonCol>
                          <IonCol></IonCol>
                          <IonCol size={"7"}>
                            <IonButton
                              color="success"
                              onClick={() => {
                                setAlert(true);
                                setIDTrans(item.id);
                              }}
                            >
                              <IonLabel class="ion-text-wrap">
                                Validasi Barang Sampai
                          </IonLabel>
                            </IonButton>
                          </IonCol>
                        </IonRow>
                      </IonGrid>
                    </IonCardContent>
                  </IonCard>
                )
              }) :
              <IonCard>
                <IonCardContent>
                  <IonCardSubtitle>Tidak ada barang yang sedang dalam perjalanan</IonCardSubtitle>
                </IonCardContent>
              </IonCard>
            : complete && complete.length > 0 ?
              complete.map((item: any) => {
                var isiComplete = item.isi.map((val: any) => {
                  return (
                    <IonGrid key={val.id}>
                      <IonRow>
                        <IonCol size={"3"}>
                          <IonImg alt="" src={val.foto} />
                        </IonCol>
                        <IonCol>
                          <IonLabel>{val.nama}</IonLabel>
                          <IonCardSubtitle>x{val.jumlah}</IonCardSubtitle>
                          <IonLabel color="danger"> Rp. {val.hargaTotal.toLocaleString()}</IonLabel>
                        </IonCol>
                      </IonRow>
                      <IonItemDivider />
                    </IonGrid>
                  )
                });
                return (
                  <IonCard key={item.id}>
                    <IonCardHeader>
                      <IonTitle> {new Date(item.date.toDate()).toLocaleString()} </IonTitle>
                    </IonCardHeader>
                    <IonCardContent>
                      {isiComplete}
                      <IonGrid>
                        <IonRow>
                          <IonCol></IonCol>
                          <IonCol size={"4"}>
                            Total Harga : <IonLabel color="danger">Rp. {item.total.toLocaleString()}</IonLabel>
                          </IonCol>
                        </IonRow>
                      </IonGrid>
                    </IonCardContent>
                  </IonCard>
                )
              }) :
              <IonCard>
                <IonCardContent>
                  <IonCardSubtitle>Anda belum pernah melakukan transaksi</IonCardSubtitle>
                </IonCardContent>
              </IonCard>
          }

        </IonContent>
      }
    </IonPage>
  )
}

export default History;