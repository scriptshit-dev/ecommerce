import React, { useEffect, useState } from 'react';
import { IonPage, IonHeader, IonToolbar, IonTitle, IonButton, IonIcon, IonContent, IonCard, IonCardContent, IonCol, IonLabel, IonGrid, IonRow, IonCardSubtitle, IonItemDivider, IonCardHeader, IonImg, IonLoading, IonModal, IonItem, IonButtons, IonAlert, IonText } from '@ionic/react';
import { arrowBack, close } from 'ionicons/icons';
import { useHistory } from 'react-router';
import { useUtils } from '../../data/utils';


const Pending: React.FC = () => {
  let history = useHistory();
  const utils = useUtils();
  const [log, setLog] = useState<any[]>([]);
  const [alert, setAlert] = useState(false);
  const [alert2, setAlert2] = useState(false);
  const [modal, setModal] = useState(false);
  const [fotoBukti, setFotoBukti] = useState({})
  const [idTrans, setIDTrans] = useState('');
  const [state, setState] = useState(0);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    setLoading(true);
    async function show() {
      setLog(l => l.splice(0, 100))
      await utils.getLog();
      setLog(utils.log);
      setLoading(false);
    }
    if (!utils.user.initializing)
      show();
  }, [state, utils])

  const kirimBukti = async (e: any) => {
    e.preventDefault();
    try {
      await utils.kirimBukti(utils.user.state.uid, idTrans, fotoBukti);
      setAlert(true)
    } catch (err) {
      console.error(err);
    }

  }

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonRow>
            <IonButton fill="clear" onClick={() => {
              history.replace('/Account');
              history.go(0);
            }}>
              <IonIcon icon={arrowBack} />
            </IonButton>
            <IonTitle> Pembayaran Tertunda </IonTitle>
          </IonRow>
        </IonToolbar>
      </IonHeader>

      <IonContent>
        <IonAlert
          isOpen={alert}
          header={"Berhasil Mengunggah Bukti Pembayaran"}
          onDidDismiss={() => {
            setAlert(false);
            setModal(false);
            setState(state + 1);
          }}
          buttons={["OK"]}
        />

        <IonAlert
          isOpen={alert2}
          header={"Apakah anda yakin membatalkan pesanan?"}
          onDidDismiss={() => setAlert2(false)}
          buttons={[
            {
              text: 'Tidak',
              handler: () => setAlert2(false)
            },
            {
              text: 'Ya',
              handler: async () => {
                await utils.delLog(utils.user.state.uid, idTrans);
                setState(state + 1);
              }
            }
          ]}
        />

        <IonModal isOpen={modal}>
          <IonHeader>
            <IonToolbar>
              <IonButtons>
                <IonTitle>Kirim Bukti</IonTitle>
                <IonButton fill="clear" color="dark" onClick={() => setModal(false)} >
                  <IonIcon icon={close} />
                  <IonLabel>Close</IonLabel>
                </IonButton>
              </IonButtons>
            </IonToolbar>
          </IonHeader>
          <IonContent>
            <form onSubmit={kirimBukti} >
              <IonItem>
                <input type="file" onChange={(e: any) => setFotoBukti(e.target.files[0])} />
              </IonItem>
              <IonButton type="submit" shape="round">
                <IonLabel>Kirim</IonLabel>
              </IonButton>
            </form>
          </IonContent>
        </IonModal>

        {loading ?
          <IonLoading
            isOpen={loading}
            message={'Loading...'}
            onDidDismiss={() => setLoading(false)}
          />
          : log && log.length > 0 ?
            log.map((item: any) => {
              var isi = item.isi.map((val: any) => {
                
                return (
                  <IonGrid key={val.id}>
                    <IonRow>
                      <IonCol size={"3"}>
                        <IonImg alt="" src={val.foto} />
                      </IonCol>
                      <IonCol>
                        <IonLabel>{val.nama}</IonLabel>
                        <IonCardSubtitle>x{val.jumlah}</IonCardSubtitle>
                        <IonLabel color="danger"> Rp. {val.harga.toLocaleString()}</IonLabel>
                      </IonCol>
                    </IonRow>
                    <IonRow>
                      <IonCol>
                        <IonText style={{fontSize:"12px"}}>
                          Silahkan Melunasi Pembayaran Ke No. Rekening {val.norek}
                        </IonText>
                      </IonCol>
                    </IonRow>
                    <IonItemDivider />
                  </IonGrid>
                )
              });
              return (
                <div>
                <IonCard key={item.id}>
                  <IonCardHeader>
                    <IonTitle> {new Date(item.date.toDate()).toLocaleString()} </IonTitle>
                  </IonCardHeader>
                  <IonCardContent>
                    {isi}
                    <IonGrid>
                      <IonRow>
                        <IonCol></IonCol>
                        <IonCol size={"7"}>
                          Total Harga : <IonLabel color="danger"> Rp. {item.total.toLocaleString()}</IonLabel>
                        </IonCol>
                      </IonRow>
                      <IonRow>
                        <IonCol size={"6"}>
                          <IonButton
                            color="danger"
                            onClick={() => {
                              setIDTrans(item.id);
                              setAlert2(true);
                            }}
                          >
                            <IonLabel class="ion-text-wrap">
                              Batalkan Pembelian
                          </IonLabel>
                          </IonButton>
                        </IonCol>
                        <IonCol size={"6"}>
                          <IonButton
                            color="success"
                            onClick={() => {
                              setModal(true);
                              setIDTrans(item.id);
                            }}
                          >
                            <IonLabel class="ion-text-wrap">
                              Kirim Bukti Pembayaran
                          </IonLabel>
                          </IonButton>
                        </IonCol>
                      </IonRow>
                    </IonGrid>
                  </IonCardContent>
                </IonCard> 
                </div>
              )
            }) :
            <IonCard>
              <IonCardContent>
                <IonCardSubtitle>Tidak ada transaksi yang belum dibayar</IonCardSubtitle>
              </IonCardContent>
            </IonCard>
        }

      </IonContent>
    </IonPage>
  )
}

export default Pending;