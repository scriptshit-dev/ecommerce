import React, { useState, useEffect, useContext, createContext } from "react";
import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firebase-firestore';
import 'firebase/firebase-storage';

const config = {
  apiKey: "AIzaSyDdF8rEkIhcCPWFGj0PNfU77F-A-5JjjZ0",
  authDomain: "ecom-8157d.firebaseapp.com",
  databaseURL: "https://ecom-8157d.firebaseio.com",
  projectId: "ecom-8157d",
  storageBucket: "ecom-8157d.appspot.com",
  messagingSenderId: "764540900022",
  appId: "1:764540900022:web:c59eb020012797f518cc17",
  measurementId: "G-MZGQTFN2ZG"
};
firebase.initializeApp(config);
let db = firebase.firestore();
var storage = firebase.storage().ref();

const AppContext = createContext();

export function ProvideApp({ children }) {
  const auth = useProvideUtils();
  return <AppContext.Provider value={auth}>{children}</AppContext.Provider>;
}

export const useUtils = () => {
  return useContext(AppContext);
};

function useProvideUtils() {
  const [user, setUser] = useState(() => {
    const state = firebase.auth().currentUser;
    return { initializing: !state, state };
  });
  const [dataUser, setDataUser] = useState([]);

  const [data, setData] = useState([]);
  const [buah, setBuah] = useState([]);
  const [sayur, setSayur] = useState([]);
  const [detail, setDetail] = useState([]);
  const [cart, setCart] = useState([]);
  const [beli, setBeli] = useState([]);
  const [log, setLog] = useState([]);
  const [shipping, setShipping] = useState([]);
  const [complete, setComplete] = useState([]);

  function onChange(state) {
    setUser({ initializing: false, state });
  }

  const signin = async (email, password) => {
    const response = await firebase
      .auth()
      .signInWithEmailAndPassword(email, password);
    setUser({ initializing: false, state: response.user });
    return response.user;
  };

  const signup = async (email, password, name, address, phone) => {
    const data = {
      email: email,
      name: name,
      address: address,
      phone: phone,
    };
    const response = await firebase
      .auth()
      .createUserWithEmailAndPassword(email, password)
      .then(newUser => {
        firebase
          .firestore()
          .collection("users")
          .doc(newUser.user.uid)
          .set(data);
        newUser.user.updateProfile({
          displayName: name,
          photoURL: null
        })
      });
    setUser({ initializing: false, state: response.user });
    return response.user;
  };

  const signout = async () => {
    await firebase
      .auth()
      .signOut();
    setUser({ ...user, state: null });
  };

  const sendPasswordResetEmail = async email => {
    await firebase
      .auth()
      .sendPasswordResetEmail(email);
    return true;
  };

  const confirmPasswordReset = async (code, password) => {
    await firebase
      .auth()
      .confirmPasswordReset(code, password);
    return true;
  };

  const getDataUser = async (uid) => {
    await db.collection('users')
      .doc(uid)
      .get()
      .then((doc) => {
        dataUser.push({
          id: doc.id,
          ...doc.data()
        })
      })
    setDataUser(dataUser);
  }

  const changeEmail = async (email) => {
    await user.state.updateEmail(email);
    await db.collection('users')
      .doc(user.state.uid)
      .update({
        email: email
      })
  }

  const changePassword = async (pass) => {
    await user.state.updatePassword(pass)
  }

  const changeName = async (uid, name) => {
    await db.collection('users')
      .doc(uid)
      .update({
        name: name
      })
  }

  const changeAddress = async (uid, address) => {
    await db.collection('users')
      .doc(uid)
      .update({
        address: address
      })
  }

  const changePhone = async (uid, phone) => {
    await db.collection('users')
      .doc(uid)
      .update({
        phone: phone
      })
  }

  const getData = async () => {
    await db.collection('produk')
      .get()
      .then((snap) => {
        snap.forEach((doc) => {
          data.push({
            id: doc.id,
            ...doc.data()
          });
        })
      })
    setData(data);
  }

  const getBuah = async () => {
    try {
      await db.collection('produk')
        .where('kategori', '==', 'buah')
        .get()
        .then((snap) => {
          snap.forEach((doc) => {
            buah.push({
              id: doc.id,
              ...doc.data()
            });
          })
        })
      setBuah(buah);
    } catch (err) {
      console.error(err)
    }
  }

  const getSayur = async () => {
    try {
      await db.collection('produk')
        .where('kategori', '==', 'sayur')
        .get()
        .then((snap) => {
          snap.forEach((doc) => {
            sayur.push({
              id: doc.id,
              ...doc.data()
            });
          })
        })
      setSayur(sayur);
    } catch (err) {
      console.error(err);
    }
  }

  const getDetail = async (id) => {
    await db.collection('produk')
      .doc(id)
      .get()
      .then((doc) => {
        detail.push({
          id: doc.id,
          ...doc.data(),
        });
      })
    setDetail(detail);
  }

  const updateStock = async (id, jml) => {
    await db.collection('produk')
      .doc(id)
      .update({stok: firebase.firestore.FieldValue.increment(-jml)})
  }

  const getBeli = async (id) => {
    await db.collection('produk')
      .doc(id)
      .get()
      .then((doc) => {
        beli.push({
          id: doc.id,
          ...doc.data(),
          items: 1,
          hargaTotal: doc.data().harga,
        });
      })
    setBeli(beli);
  }

  const getCart = async (uid) => {
    try {
      await db.collection('users')
        .doc(uid)
        .collection('cart')
        .get()
        .then((snap) => {
          snap.forEach((doc) => {
            let value = doc.get("ref");
            value.get().then(item => {
              if(item.exists) {
                cart.push({
                  id: item.id,
                  check: false,
                  items: 1,
                  hargaTotal: item.data().harga,
                  ...item.data()
                });
              }
            })
          })
        }).catch((err) => {
          console.error(err);
        })
      setCart(cart);
    } catch (err) {
      console.error(err)
    }
  }

  const toCart = async (uid, id) => {
    let get = db.doc('produk/' + id);
    let refs = db.doc(get.path);
    let data = {
      ref: refs
    };

    try {
      await db.collection('users')
        .doc(uid)
        .collection('cart')
        .doc(id)
        .set(data);
    } catch (err) {
      console.error(err);
    }
  }

  const delCart = async (id) => {
    try {
      await db.collection('users')
        .doc(user.state.uid)
        .collection('cart')
        .doc(id)
        .delete();
    } catch (err) {
      console.error(err);
    }
  }

  const bayar = async (uid, isi, penerima, total) => {
    let data = {
      isi: isi,
      total: total,
      date: firebase.firestore.Timestamp.fromDate(new Date()),
      status: "Pending",
      fotoBukti: "",
      ...penerima,
    }
    try {
      await db.collection('users')
        .doc(uid)
        .collection('transaksi')
        .add(data);
    } catch (err) {
      console.error(err)
    }
  }

  const getLog = async () => {
    await db.collection('users')
      .doc(user.state.uid)
      .collection('transaksi')
      .where('status', '==', 'Pending')
      .get()
      .then((snap) => {
        snap.forEach((doc) => {
          log.push({
            id: doc.id,
            ...doc.data()
          });
        })
      })
    setLog(log);
  }

  const delLog = async (uid, id) => {
    await db.collection('users')
      .doc(uid)
      .collection('transaksi')
      .doc(id)
      .delete();
  }

  const getShipping = async () => {
    await db.collection('users')
      .doc(user.state.uid)
      .collection('transaksi')
      .where('status', '==', 'Shipping')
      .get()
      .then((snap) => {
        snap.forEach((doc) => {
          shipping.push({
            id: doc.id,
            ...doc.data()
          });
        })
      })
    setShipping(shipping);
  }

  const shipToComp = async (uid, id) => {
    try {
      await db.collection('users')
        .doc(uid)
        .collection('transaksi')
        .doc(id)
        .update({
          status: "Complete"
        });
    } catch (err) {
      console.error(err);
    }
  }

  const getComplete = async () => {
    await db.collection('users')
      .doc(user.state.uid)
      .collection('transaksi')
      .where('status', '==', 'Complete')
      .get()
      .then((snap) => {
        snap.forEach((doc) => {
          complete.push({
            id: doc.id,
            ...doc.data()
          });
        })
      })
    setComplete(complete);
  }

  const kirimBukti = async (uid, id, foto) => {
    let imageRef = storage.child("bukti/" + uid + "/" + id);
    await imageRef.put(foto)
      .then((snap) => {
        snap.ref.getDownloadURL().then(async (downloadURL) => {
          let data = {
            fotoBukti: downloadURL,
            status: "Process"
          }
          try {
            await db.collection('users')
              .doc(uid)
              .collection('transaksi')
              .doc(id)
              .update(data)
          } catch (err) {
            console.error(err)
          }

        })
      })

  }

  useEffect(() => {
    const unsubscribe = firebase.auth().onAuthStateChanged(onChange);

    return () => unsubscribe();
  }, []);

  return {
    user,
    dataUser,
    data,
    buah,
    sayur,
    detail,
    beli,
    cart,
    log,
    shipping,
    complete,
    signin,
    signup,
    signout,
    sendPasswordResetEmail,
    confirmPasswordReset,
    getDataUser,
    changeEmail,
    changePassword,
    changeName,
    changeAddress,
    changePhone,
    getData,
    getBuah,
    getSayur,
    getDetail,
    updateStock,
    getBeli,
    getCart,
    toCart,
    delCart,
    bayar,
    getLog,
    delLog,
    getShipping,
    shipToComp,
    getComplete,
    kirimBukti,
  };
}